import { SecurityService } from './SecurityService';
declare function AuthorizeDirective($security: SecurityService): ng.IDirective;
export default AuthorizeDirective;
